"""
A curation script that fiddle file.type for DICOM files.
Could be use to relaunch gear rules on a DICOM files assuming a
gear-rule is set on file.type = dicom.
"""

import logging

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger(__name__)


def is_dicom(file_: flywheel.FileEntry):
    if file_.type == "dicom":
        return True
    return False


def fiddle_file_type(acq: flywheel.Acquisition, file_: flywheel.FileEntry):
    type_ = file_.type
    # Set to none type
    acq.update_file(file_.name, {"type": None})
    # Set back to original
    res = acq.update_file(file_.name, {"type": type_})
    log.info(f"{file_.name}: {res}")


class Curator(HierarchyCurator):

    # Curate acquisition files
    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        log.info(f"Curating acquisition {acquisition.label}")
        acq = acquisition.reload()
        for file_ in acq.files:
            if is_dicom(file_):
                log.info(f"Retriggering {file_.name}")
                fiddle_file_type(acq, file_)
