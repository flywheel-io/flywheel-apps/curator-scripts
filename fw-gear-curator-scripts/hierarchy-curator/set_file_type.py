import logging
import os
import pathlib
import re
import subprocess
import sys
import backoff

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

log = logging.getLogger(__name__)

FILETYPES = {
    "bval": [".bval", ".bvals"],
    "bvec": [".bvec", ".bvecs"],
    "dicom": [".dcm", ".dcm.zip", ".dicom.zip", ".dicom"],
    "eeg": [".eeg.zip"],
    "eeg data": [".eeg"],
    "eeg marker": [".vmrk"],
    "eeg header": [".vhdr"],
    "gephysio": [".gephysio.zip"],
    "HDF5": [".h5", ".hdf5"],
    "MATLAB data": [".mat"],
    "MGH data": [".mgh", ".mgz", ".mgh.gz"],
    "nifti": [".nii.gz", ".nii", ".nifti"],
    "ParaVision": [".pv5.zip", ".pv6.zip"],
    "parrec": [".parrec.zip", ".par-rec.zip"],
    "pfile": [".7.gz", ".7", ".7.zip"],
    "PsychoPy data": [".psydat"],
    "qa": [".qa.png", ".qa.json", ".qa.html"],
    "archive": [
        ".zip",
        ".tbz2",
        ".tar.gz",
        ".tbz",
        ".tar.bz2",
        ".tgz",
        ".tar",
        ".txz",
        ".tar.xz",
    ],
    "document": [".docx", ".doc"],
    "image": [".jpg", ".tif", ".jpeg", ".gif", ".bmp", ".png", ".tiff"],
    "markup": [".html", ".htm", ".xml"],
    "markdown": [".md", ".markdown"],
    "log": [".log"],
    "pdf": [".pdf"],
    "presentation": [".ppt", ".pptx"],
    "source code": [
        ".c",
        ".py",
        ".cpp",
        ".js",
        ".m",
        ".json",
        ".java",
        ".php",
        ".css",
        ".toml",
        ".yaml",
        ".yml",
        ".sh",
    ],
    "spreadsheet": [".xls", ".xlsx"],
    "tabular data": [".csv.gz", ".csv", ".tsv.gz", ".tsv"],
    "text": [".txt"],
    "video": [".mpeg", ".mpg", ".mov", ".mp4", ".m4v", ".mts"],
    "3D Object": [".obj"],
    "Material Library": [".mtl"],
    "Cinema 4D Model": [".c4d"],
    "CG Resource": [".cgresource.zip"],
    "ITK MetaIO Header": [".mhd"],
    "Raw": [".raw"],
}

FILETYPE_LOOKUP = {}
for key, values in FILETYPES.items():
    for value in values:
        FILETYPE_LOOKUP[value] = key


@backoff.on_exception(backoff.expo, flywheel.rest.ApiException, max_time=300)
def update_type_and_modality(cont, file_):
    if file_.type:
        if file_.modality and file_.classification:
            log.info("File type and modality are already set, skipping.")
            return
        elif file_.modality is None and file_.classification is {}:
            log.info("File type is set and no modality to restore, skipping.")
            return

    if file_.modality is None:
        modality = None
        if file_.classification:
            # need to find the matching modality
            if "OCT Type" in file_.classification:
                modality = "OCT"
            elif "Sub-Type" in file_.classification or "Type" in file_.classification:
                modality = "FP"
    else:
        modality = file_.get('modality', None)

    filename = file_.name
    particles = filename.split(".")[1:]
    extensions = ["." + ".".join(particles[i:]) for i in range(len(particles))]
    filetype = None
    for ext in extensions:
        filetype = FILETYPE_LOOKUP.get(ext.lower())
        if filetype:
            break
    log.info(f'Udpating file {filename}, type={filetype}, modality={modality}')
    cont.update_file(
        filename, type=filetype, modality=modality
    )


def process_container(cont):
    for file_ in cont.files:
        update_type_and_modality(cont, file_)


class Curator(HierarchyCurator):
    extra_packages = []

    def curate_project(self, project):
        log.info(f"Processing files in project container {project.id}...")
        process_container(project)

    def curate_subject(self, subject):
        log.info(f"Processing files in subject container {subject.id}...")
        process_container(subject)

    def curate_session(self, session):
        log.info(f"Processing files in session container {session.id}...")
        process_container(session)

    def curate_acquisition(self, acquisition):
        log.info(f"Processing files in acquisition container {acquisition.id}...")
        process_container(acquisition)
