"""
A curation script that fiddle file.type for DICOM files that have no job
with GEAR_NAME:MIN_VERSION ran on them.

Requirements:
* gear-rule set for file.type DICOM files with the appropriate GEAR_NAME:MIN_VERSION
"""

import logging

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator
from packaging import version

log = logging.getLogger(__name__)

GEAR_NAME = "dicom-mr-classifer"
MIN_VERSION = "1.3.2"


def is_dicom(file_: flywheel.FileEntry):
    if file_.type == "dicom":
        return True
    return False


def is_file_processed_with_gear_supeq_version(
    client: flywheel.Client, file_: flywheel.FileEntry
):
    jobs = client.jobs.find(f"parents.session={file_.parents.session}")
    for j in jobs:
        if (
            j.state == "complete"
            and j.gear_info.name == GEAR_NAME
            and version.parse(j.gear_info.version) >= version.parse(MIN_VERSION)
            and j.destination.id == file_.parents.acquisition
            and len(j.inputs) > 0
            and j.inputs[0].name == file_.name
        ):
            return True
    return False


def fiddle_file_type(acq: flywheel.Acquisition, file_: flywheel.FileEntry):
    type_ = file_.type
    # Set to none type
    acq.update_file(file_.name, {"type": None})
    # Set back to original
    res = acq.update_file(file_.name, {"type": type_})
    log.info(f"{file_.name}: {res}")


class Curator(HierarchyCurator):

    # Curate acquisition files
    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        log.info(f"Curating acquisition {acquisition.label}")
        acq = acquisition.reload()
        for file_ in acq.files:
            if is_dicom(file_) and not is_file_processed_with_gear_supeq_version(
                self.context.client, file_
            ):
                log.info(f"Retriggering {file_.name}")
                fiddle_file_type(acq, file_)
