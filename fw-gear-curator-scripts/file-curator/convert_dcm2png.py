"""File-curator to convert single frame dicom (.dcm) into (.png)"""
import logging
import os
import subprocess
import sys
from pathlib import Path
from typing import Any, Dict

from flywheel_gear_toolkit.utils.curator import FileCurator

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class Converter:

    folder = None
    suffix = None

    def convert(self, input_file, dest_file):
        raise NotImplemented


class ConverterDcm2Png(Converter):

    folder = "png"
    suffix = ".png"

    def convert(self, input_file, dest_file):
        import pydicom
        from PIL import Image

        dicom = pydicom.read_file(input_file, force=True)
        img = Image.fromarray(dicom.pixel_array)
        if not dest_file.suffix == self.suffix:
            dest_file = f"{dest_file}{self.suffix}"
        img.save(dest_file)
        return dest_file


class Curator(FileCurator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.client = self.context.client
        self.extra_packages = ["Pillow", "pydicom"]

    def _import_module(self):
        converter_file = Path(__file__).absolute()
        path, fname = os.path.split(converter_file)
        sys.path.append(path)

    def _install_extra_package(self):
        for p in self.extra_packages:
            subprocess.check_call([sys.executable, "-m", "pip", "install", p])

    def curate_file(self, file_: Dict[str, Any]):

        if file_.get("object").get("type") == "dicom" and not file_.get("location").get(
            "name"
        ).endswith(".zip"):
            log.info(f"Converting file {file_.get('location').get('name')} to png")
            dest_file = file_.get("location").get("name")
            if dest_file.endswith(".dcm"):
                dest_file = dest_file.split(".dcm")[0] + ".png"
            else:
                dest_file = dest_file + ".png"
            dest_path = self.context.output_dir / dest_file

            converter = ConverterDcm2Png()
            try:
                converter.convert(file_.get("location").get("path"), dest_path)
            except Exception:
                log.exception("Exception raised when converting dcm to png")
                sys.exit(1)

            file_object = file_.get("object", {})
            file_object_info = file_.get("object", {}).get("info", {})
            if "header" in file_object_info:
                file_object_info.pop("header")
            self.context.update_file_metadata(
                dest_file,
                modality=file_object.get("modality"),
                classification=file_object.get("classification"),
                info=file_object_info,
            )
