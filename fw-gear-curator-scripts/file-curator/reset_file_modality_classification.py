import logging
from typing import Any, Dict

from flywheel_gear_toolkit.utils.curator import FileCurator

log = logging.getLogger(__name__)


class Curator(FileCurator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.client = self.context.client

    def curate_file(self, file_: Dict[str, Any]):

        log.info(f"Curating file {file_.get('location').get('name')}")

        if file_.get("object").get("type") == "dicom":

            # File Attribute
            file_name = file_.get("location").get("name")
            modality = file_.get("modality")

            if modality:
                classification = self.client.get_modality(modality)["classification"]
                empty_file_classification = {k: [] for k in classification.keys()}
                empty_file_classification.update({"Custom": []})
            else:
                empty_file_classification = {"Custom": []}

            self.context.update_file_metadata(
                file_name, {"classification": empty_file_classification}
            )
            log.info(f"Updated... Exiting...")
        else:
            log.error("Input file is not a DICOM file type. Exiting...")
