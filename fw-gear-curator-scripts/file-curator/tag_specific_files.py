"""File-curator to tag a file based on file attributes"""
import logging
from typing import Any, Dict

from flywheel_gear_toolkit.utils.curator import FileCurator

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class Curator(FileCurator):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.client = self.context.client

    def curate_file(self, file_: Dict[str, Any]):
        """Tag the file with ``"Curated"`` if file attributes meets specific criteria"""
        type_ = file_.get("object").get("type")
        modality = file_.get("object").get("modality")
        classification = file_.get("object").get("classification")

        if (
            type_ == "dicom"
            and modality == "MR"
            and "T1" in classification.get("Measurement")
            and "Structural" in classification.get("Intent")
        ):
            tags = file_.get("object").get("tags")
            tags.append("Curated")
            self.context.update_file_metadata(file_.get("location").get("name"),
                                              tags=tags)
