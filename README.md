# curator-scripts

A repository that stores curation scripts for 
[file-curator](https://gitlab.com/flywheel-io/flywheel-apps/file-curator) and 
[hierarchy-curator](https://gitlab.com/flywheel-io/flywheel-apps/hierarchy-curator) gears.


file-curator compatible scripts can be found in 
[fw-gear-curator-scripts/file-curator](./fw-gear-curator-scripts/file-curator) folder.

hierarchy-curator compatible scripts can be found in 
[fw-gear-curator-scripts/hierarchy-curator](./fw-gear-curator-scripts/hierarchy-curator) 
folder.
